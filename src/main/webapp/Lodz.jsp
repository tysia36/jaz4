<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ page import="domain.City" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Pogoda w Łodzi</title>
</head>
<body>

<jsp:useBean id="apiService" class="services.CityApiService" scope="application"></jsp:useBean>

<a href="index.jsp"> Wróć do strony głównej, aby wybrać inne miasto.</a>
<h1>Pogoda w Łodzi:</h1>

<%
	City returned = apiService.getCity(3093133); 
	double temp =returned.getMain().getTemp();
	double tempMin =returned.getMain().getTemp_min();
	double tempMax =returned.getMain().getTemp_max();
	double pressure =returned.getMain().getPressure();
	double humidity = returned.getMain().getHumidity();
	double speed = returned.getWind().getSpeed();
%>

<h3>Ogólne:</h3>
<p> Temperatura: <%= temp %> &deg;C</p>
<p> Temperatura minimalna: <%= tempMin %> &deg;C</p>
<p> Temperatura maksymalna: <%= tempMax %> &deg;C</p>
<p> Ciśnienie: <%= pressure %> hPa</p>
<p> Zachmurzenie: <%=humidity %> %</p>
<p> Prędkość wiatru: <%=speed %> m/s </p>
</body>
</html>