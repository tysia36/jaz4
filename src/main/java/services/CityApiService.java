package services;

import java.io.IOException;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import domain.City;

@XmlRootElement
public class CityApiService 
{
	private String url1="http://api.openweathermap.org/data/2.5/weather?id=";
	private String url2="&appid=6e21a20737c4aed26a325c95c2cf8069&units=metric&lang=pl";
	
	public City getCity(int id) throws JsonParseException, JsonMappingException, IOException
	{
		
		ClientConfig clientConfig = new DefaultClientConfig();
		Client client = Client.create(clientConfig);
		
		String url = ""+url1 +id +url2;
		WebResource webResource =client.resource(url);
		
		String response = webResource.get(String.class);
		
		ObjectMapper mapper = new ObjectMapper();
		City res = mapper.readValue(response, City.class);
		
		return res;
	}

}
