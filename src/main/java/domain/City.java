package domain;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class City 
{
	private CoordinatesForCity coord;
	private List<WeatherForCity> weather;
	private String base;
	private MainForCity main;
	private WindForCity wind;
	private Map<String, String> rain;
	private CloudsForCity clouds;
	private int dt;
	private SysForCity sys;
	private int id;
	private String name;
	private int cod;
	
	public CoordinatesForCity getCoord() {
		return coord;
	}
	public void setCoord(CoordinatesForCity coord) {
		this.coord = coord;
	}
	public List<WeatherForCity> getWeather() {
		return weather;
	}
	public void setWeather(List<WeatherForCity> weather) {
		this.weather = weather;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public MainForCity getMain() {
		return main;
	}
	public void setMain(MainForCity main) {
		this.main = main;
	}
	public WindForCity getWind() {
		return wind;
	}
	public void setWind(WindForCity wind) {
		this.wind = wind;
	}
	/*public RainForCity getRain() {
		return rain;
	}
	public void setRain(RainForCity rain) {
		this.rain = rain;
	}*/
	public CloudsForCity getClouds() {
		return clouds;
	}
	public Map<String, String> getRain() {
		return rain;
	}
	public void setRain(Map<String, String> rain) {
		this.rain = rain;
	}
	public void setClouds(CloudsForCity clouds) {
		this.clouds = clouds;
	}
	public int getDt() {
		return dt;
	}
	public void setDt(int dt) {
		this.dt = dt;
	}
	public SysForCity getSys() {
		return sys;
	}
	public void setSys(SysForCity sys) {
		this.sys = sys;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
}
